package com.squad4.finance.util;

public class CustomCode {
	
	
	/**
	 * Exception Code
	 */
	public static final String CUSTOMER_NOT_FOUND_EXCEPTION_MESSAGE = "Customer Not Found ";
	public static final String CUSTOMER_NOT_FOUND_EXCEPTION_CODE = "EXC001";
	public static final String TRANSACTION_NOT_FOUND_EXCEPTION_MESSAGE = "Transaction Not Found";
	public static final String TRANSACTION_NOT_FOUND_EXCEPTION_CODE = "EXC002";
	
	/**
	 * Successful Code And Message
	 */
	
	public static final String TRANSACTION_SUCCESSFUL_MESSAGE = "Transaction completed successfully";
	public static final String TRANSACTION_SUCCESSFUL_CODE= "SUCCESS001";
	public static final String TRANSACTION_DETAILS = "Customer Transaction Detail Fetched successfully";
	public static final String TRANSACTION_DETAILS_CODE= "SUCCESS002";
	public static final String MONTHLY_TRANSACTION_DETAILS ="Customer Monthly Transaction Detail Fetched successfully";
	public static final String MONTHLY_TRANSACTION_CODE ="SUCCESS003";
	public static final String FUTURE_EXPENSE_MESSAGE ="Future Expenses Detail Fetched successfully";
	public static final String FUTURE_EXPENSE_CODE="SUCCESS004";
	

	private CustomCode() {
		super();
	}
}

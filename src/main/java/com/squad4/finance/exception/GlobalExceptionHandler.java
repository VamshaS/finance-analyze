package com.squad4.finance.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleCustomerNotFoundException(CustomerNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	
	@ExceptionHandler(TransactionNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleTransactionNotFoundException(TransactionNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}

	
}

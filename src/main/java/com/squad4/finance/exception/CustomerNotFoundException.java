package com.squad4.finance.exception;

import com.squad4.finance.util.CustomCode;

public class CustomerNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException() {
		super(CustomCode.CUSTOMER_NOT_FOUND_EXCEPTION_MESSAGE,CustomCode.CUSTOMER_NOT_FOUND_EXCEPTION_CODE);
		
	}

}

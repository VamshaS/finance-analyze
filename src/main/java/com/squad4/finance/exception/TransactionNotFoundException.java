package com.squad4.finance.exception;

import com.squad4.finance.util.CustomCode;

public class TransactionNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TransactionNotFoundException() {
		super(CustomCode.TRANSACTION_NOT_FOUND_EXCEPTION_MESSAGE,CustomCode.TRANSACTION_NOT_FOUND_EXCEPTION_CODE);
		
	}

}

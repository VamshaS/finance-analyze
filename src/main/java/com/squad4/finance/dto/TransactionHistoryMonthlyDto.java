package com.squad4.finance.dto;

import java.util.List;

public record TransactionHistoryMonthlyDto(ApiResponse apiResponse,List<MonthlyTransactionDto> monthlyTransactionDtos) {

}

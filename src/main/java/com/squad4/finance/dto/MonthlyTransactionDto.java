package com.squad4.finance.dto;

import lombok.Builder;

@Builder
public record MonthlyTransactionDto(String month,
		Double totalIncoming,
		Double totalOutgoing,
		Double closingBalance) {

}

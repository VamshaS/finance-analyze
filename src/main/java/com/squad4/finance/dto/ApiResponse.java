package com.squad4.finance.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,String code) {

}

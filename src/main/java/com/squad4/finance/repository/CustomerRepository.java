package com.squad4.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.finance.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}

package com.squad4.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.finance.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
